package com.example.homeworkspring.service.imp;

import com.example.homeworkspring.model.Article;
import com.example.homeworkspring.repositery.ArticleRepositery;
import com.example.homeworkspring.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService {

    @Autowired
    ArticleRepositery articleRepositery;

    @Override
    public List<Article> getAll() {
        return articleRepositery.getAll();
    }

    @Override
    public List<Article> delete(int id) {
        return articleRepositery.delete(id);
    }

    @Override
    public List<Article> upload(Article article) {
        return articleRepositery.upload(article);
    }

    @Override
    public Article findById(int id) {
        return articleRepositery.findById(id);
    }

}
