package com.example.homeworkspring.service;

import com.example.homeworkspring.model.Article;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public interface ArticleService {

    List<Article> getAll();
    List<Article> delete(int id);
    List<Article> upload(Article article);
    Article findById(int id);

}
