package com.example.homeworkspring;

import com.example.homeworkspring.model.Article;
import com.example.homeworkspring.repositery.ArticleRepositery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class HomeworkspringApplication {

    public static void main(String[] args) {

        SpringApplication.run(HomeworkspringApplication.class, args);

    }

}
