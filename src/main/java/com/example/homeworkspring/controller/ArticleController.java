package com.example.homeworkspring.controller;


import com.example.homeworkspring.model.Article;
import com.example.homeworkspring.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


@Controller
public class ArticleController {

    @Autowired
    ArticleService articleService;

    @GetMapping("/home")
    public String home(Model model){
        model.addAttribute("articles", articleService.getAll());
        return "home";
    }

    @GetMapping("/delete/{id}")
    public String delete(Model model, @PathVariable int id){

                model.addAttribute("articles", articleService.delete(id));
        return "redirect:/home";
    }

    @GetMapping("/create")
    public String create(){
        return "create";
    }

    @PostMapping("/article/create")
    public String upload(
                         @RequestParam String title,
                         @RequestParam String description,
                         @RequestParam("image") MultipartFile multipartFile,
                         Model model
    ) throws IOException {

        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        Article article = new Article(title,description,fileName);
        Path uploadPath = Paths.get("src/main/resources/photos");
        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
            model.addAttribute("articles", articleService.upload(article));
        } catch (IOException ioe) {
            throw new IOException("Could not save image file: " + fileName, ioe);
        }

        return "redirect:/home";
    }

    @GetMapping("/view/{id}")
    public String view(Model model, @PathVariable int id){

        model.addAttribute("article", articleService.findById(id));
        return "detail";
    }


    @GetMapping("/edit/{id}")
    public String edit(Model model,@PathVariable int id){
        model.addAttribute("article", articleService.findById(id));
        return "edit";


    }

    @PostMapping("/article/edit/{id}")
    public String update(@PathVariable int id,
                         Model model,
                         @RequestParam String title,
                         @RequestParam String description,
                         @RequestParam("image") MultipartFile multipartFile
    ) throws IOException {
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        Path uploadPath = Paths.get("src/main/resources/photos");
        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);

            articleService.findById(id).setTitle(title);
            articleService.findById(id).setDescription(description);
            articleService.findById(id).setImage(fileName);
            model.addAttribute("articles", articleService);
        } catch (IOException ioe) {
            throw new IOException("Could not save image file: " + fileName, ioe);
        }

        return "redirect:/home";
    }
}
