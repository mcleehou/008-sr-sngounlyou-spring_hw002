package com.example.homeworkspring.repositery;


import com.example.homeworkspring.model.Article;
import com.example.homeworkspring.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class ArticleRepositery {

    @Autowired
    private ArticleService articleService;

    List<Article> articleList ;
//            = Arrays.asList(
//            new Article(1,"New Title" , "This is new title", "image/abc.jpeg"),
//            new Article(2,"New Title" , "This is new title", "image/abc.jpeg"),
//            new Article(3,"New Title" , "This is new title", "image/abc.jpeg")
//    );

    ArticleRepositery(){
        articleList = new ArrayList<>();
        articleList.add(new Article("New Title" , "This is New Descriptions","images.jpg"));
        articleList.add(new Article("New Title" , "This is New Descriptions","images.jpg"));
        articleList.add(new Article("New Title" , "This is New Descriptions","images (1).jpg"));

    }

    public List<Article> getAll(){
        return articleList;
    }

    public List<Article> delete(int id){
        articleList.removeIf(article -> article.getId()== id);
        return articleList;
    }

    public List<Article> upload(Article article){
        articleList.add(article);
        return articleList;
    }

    public Article findById(int id){
        Article article1 = new Article("","","");
        for (Article article : articleList)
            if (article.getId() == id)
                 article1 = article;
        return article1;
    }

}
